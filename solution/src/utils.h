#include <stdint.h>
#include <stdio.h>


int64_t open_file_read(const char* filename, FILE** file);
int64_t open_file_write(const char* filename, FILE** file);

void log_error(const char* msg, int64_t err);
