#include "image_utils.h"

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>


struct __attribute__((__packed__))  pixel {
	uint8_t b, g, r;
};

void update_image(struct image* img, uint64_t width, uint64_t height, struct pixel* const data){
	img -> width = width;
	img -> height = height;
	img -> data = data;
}

void free_image(struct image* img){
	free(img -> data);
	img -> data = NULL;
}

struct pixel* new_pixels(size_t width, size_t height){
	struct pixel* pixels = malloc(width * height * sizeof(struct pixel));
	return pixels;
}

void copy_image(struct image const img, struct image* dest){
	if (dest == NULL)
	  return;
	dest -> data = new_pixels(img.width, img.height);
	if (dest->data == NULL)
	{
	  dest->width = dest->height = 0;
	  return;
	}
	dest->width = img.width;
	dest->height = img.height;
	for(uint64_t y = 0; y < img.height; y++){
		for(uint64_t x = 0; x < img.width; x++){
			dest -> data[y * img.width + x] = img.data[y * img.width + x];
		}
	}
}

size_t calc_pixels_line_size(uint64_t width){
	return width * sizeof(struct pixel);
}

uint64_t get_width(struct image const img){
	return img.width;
}

uint64_t get_height(struct image const img){
	return img.height;
}

struct pixel* get_pixels(struct image const img, ssize_t line){
	if(line == -1)
		return img.data;
	else
		return img.data + (line * img.width);
}

struct pixel* get_pixel(struct image const img, uint64_t x, uint64_t y){
	return img.data + (y * img.width + x);
}

void set_pixel(struct image img, uint64_t x, uint64_t y, struct pixel const* pix){
	struct pixel* p = img.data + (y * (img.width) + x);
	*p = *pix;
}
