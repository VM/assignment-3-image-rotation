#include "utils.h"

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int64_t open_file_read(const char* filename, FILE** file){
	errno = 0;
	*file = fopen(filename, "rb");
	return (int64_t)errno;
}
int64_t open_file_write(const char* filename, FILE** file){
	errno = 0;
	*file = fopen(filename, "wb");
	return (int64_t)errno;
}

void log_error(const char* msg, int64_t err){
	if(err != 0)
		fprintf(stderr, "ERROR WITH FILE %s: %s\n", msg, strerror((int)err));
}
