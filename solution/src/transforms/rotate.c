#include "rotate.h"
#include "../image_utils.h"

#include <stdint.h>
#include <stdio.h>

#define ROTATE_PI_BY_2_LOOP for(uint64_t y = 0; y < width; y++) \
								for(uint64_t x = 0; x < height; x++)

#define ROTATE_PI_LOOP 	for(uint64_t y = 0; y < height; y++) \
							for(uint64_t x = 0; x < width; x++)

struct image rotate(struct image const img, int angle){
	struct image rotated_img = {0};

	const uint64_t width = get_width(img);
	const uint64_t height = get_height(img);

	if(angle % 180 == 0)
		copy_image(img, &rotated_img);
	else
		update_image(&rotated_img, height, width, new_pixels(height, width));

	if(rotated_img.data == NULL)
		return (struct image){0};

	switch(angle){
		case 90:
			ROTATE_PI_BY_2_LOOP
				set_pixel(rotated_img, x, width - y - 1, get_pixel(img, y, x));
			break;
		case 180:
			ROTATE_PI_LOOP
				set_pixel(rotated_img, width - x - 1, height - y - 1, get_pixel(img, x, y));
			break;
		case 270:
			ROTATE_PI_BY_2_LOOP
				set_pixel(rotated_img, height - x - 1, y, get_pixel(img, y, x));
			break;
	}
	return rotated_img;
}
