#include "bmp_format.h"
#include "image_utils.h"
#include "transforms/rotate.h"
#include "utils.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char** argv ) {

    if(argc != 4){
		fprintf(stderr, "Usage: %s <input> <output> <angle>\n", argv[0]);
		return 1;
	}
    const char* input = argv[1];
	const char* output = argv[2];
	int angle = (360 + atoi(argv[3]) % 360) % 360;

    FILE* bmp = 0;
    log_error(input, open_file_read(input, &bmp));
    struct image img;
    if(bmp == 0)
    	return 1;

    enum read_status status = from_bmp(bmp, &img);
    fclose(bmp);
    if(status != READ_OK){
    	switch(status){
			case READ_INVALID_SIGNATURE:
				fprintf(stderr, "Invalid signature of file\n");
				break;
			case READ_INVALID_BITS:
				fprintf(stderr, "Invalid bits per pixel, only 24 b/p is supported\n");
				break;
			case READ_INVALID_HEADER:
				fprintf(stderr, "Invalid header\n");
				break;
			case READ_INVALID_PIXELS:
				fprintf(stderr, "Invalid pixels array\n");
				break;
			case READ_ALLOCATION_ERROR:
				fprintf(stderr, "Error allocate memory to load image\n");
				break;
			default:
				fprintf(stderr, "Unknown error\n");
				break;
    	}
		return 1;
    }

    struct image img1 = {0};
    if(angle % 90 != 0){
    	fprintf(stderr, "Invalid angle\n");
    	free_image(&img);
    	return 1;
    }
    img1 = rotate(img, angle);
    free_image(&img);

    if(img1.data == 0){
    	fprintf(stderr, "Error allocate memory to rotate image\n");
    	return 1;
    }

    FILE* out = 0;
    log_error(output, open_file_write(output, &out));
    if(out == 0){
    	free_image(&img1);
    	return 1;
    }
    enum write_status write_status = to_bmp(out, &img1);
	fclose(out);
	if(img1.data != 0)free_image(&img1);
	if(write_status != WRITE_OK){
		fprintf(stderr, "Error writing file\n");
		return 1;
	}

    return 0;
}
